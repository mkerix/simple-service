jest.mock('../../package.json', () => {
    return {
        version: "1.2.3"
    }
}, { virtual: true });

import { ServiceBroker, Errors } from "moleculer";
import VersionService from "../../services/version.service";

describe("Test 'version' service", () => {
    let broker = new ServiceBroker();
    broker.createService(VersionService);

    beforeAll(() => broker.start());
    afterAll(() => broker.stop());

    describe("Test 'version.version' action", () => {

        it('should return the current version from package.json', () => {
            return expect(broker.call("version.version")).resolves.toBe("1.2.3")
        });

    })
});
