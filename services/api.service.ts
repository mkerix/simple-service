import { ServiceSchema } from "moleculer";
import ApiGateway = require("moleculer-web");

const ApiService: ServiceSchema = {
	name: "api",

	mixins: [ApiGateway],

	// More info about settings: https://moleculer.services/docs/0.13/moleculer-web.html
	settings: {
		port: process.env.PORT || 3000,

		routes: [{
			mappingPolicy: "restrict",
			aliases: {
				"GET /health"(req: any, res: any): void {
					res.end('ok')
				},
				"GET version": "version.version"
			}
		}]
	}
};

export = ApiService;
