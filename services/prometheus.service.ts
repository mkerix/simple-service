import { ServiceSchema } from "moleculer";
import PromService from "moleculer-prometheus";

const PrometheusService: ServiceSchema = {
    name: "prometheus",

    mixins: [PromService]
};

export = PrometheusService;
