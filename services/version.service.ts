import { ServiceSchema } from "moleculer";
import { version } from "../package.json";

const VersionService: ServiceSchema = {
    name: "version",

    actions: {
        version(): string {
            return version;
        }
    }
};

export = VersionService;
