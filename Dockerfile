# Debian-based Node image required for building native dependencies
FROM node:12 AS ts-builder

WORKDIR /app

# Only copy package definitions for caching reasons
# -> if files do not change, we do not need to reinstally dependencies
COPY package*.json /app/
RUN npm ci

# Only prod modules are required for running
COPY . /app/
RUN npm run build && npm prune --production

FROM node:12-alpine

# Copy over ready-to-use app to reduce layers
WORKDIR /app
COPY --from=ts-builder /app/ /app/

ENV NODE_ENV=production
CMD ["npm", "start"]
