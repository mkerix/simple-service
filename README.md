# simple-service

## Getting started

### Developing this service

To develop this service you will need:

- Node 12.x (LTS)
- npm

Clone the repository from GitLab and run `npm install` from the cloned folder. You are now ready to go. 
`npm run dev` will run an instance of the service that hot reloads on any changes you make. You will be able to access the local instance at `localhost:3000`.
A docker-compose file is also included for convenience, to run this service using Docker just type in `docker-compose up`.

This repository uses [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/), please structure your commit messages accordingly.

### Releasing this service

To test the release process locally you can run `npm run build` and `npm start`. 

Proper releases of the service should only be done by the CI. Please create a new semver tag in git to begin the release process.
The image will be tagged as semver and then deployed using a blue/green deployment of 50%. After 10 minutes the release is pushed to 100%.

## Reference

### Endpoints

Current endpoints are:

- `GET /version` - returns current version as defined in package.json
- `GET /health` - return `ok`

This project also serves [Prometheus metrics](https://github.com/moleculerjs/moleculer-metrics/tree/master/packages/moleculer-prometheus) under port 3030.

### Project structure

The project is based off the [official quickstart project](https://github.com/moleculerjs/moleculer-template-project-typescript).
You can find all services, written according to the Moleculer service schema, in the `service` folder. Tests are under the `tests` folder.
The `chart` folder contains a basic Helm chart used for deploying this application.
The project is automatically tested using Jest and linted using ESLint in the CI, see `.gitlab-ci.yml`. All other scripts for deployment can also be found in there.

The service is made accessible to the outside world with an Ingress, as it is purely HTTP based.
Internally you can access the application via its ClusterIP Service record.
For canary deployments nginx-ingress is needed, as we use their annotations to do this.

### NPM scripts

- `npm run dev` - Start development mode (load all services locally with hot-reload & REPL)
- `npm run build`- Uses typescript to transpile service to javascript
- `npm start` - Start production mode (set `SERVICES` env variable to load certain services) (previous build needed)
- `npm run cli`: Start a CLI and connect to production. Don't forget to set production namespace with `--ns` argument in script
- `npm test` - Run tests & generate coverage report


## Future considerations

### Missing for prod deployment

- Fix canary deployments
- Fix Kaniko cache (caches old code currently)
- Proper liveness/readiness checks
- Proper resource requests and limits
- Metrics to gauge whether staged rollout should proceed
- Deployments to a staging environment for testing purposes
- Regenerated Kube credentials (exposed in some failed CI logs)
- Fix critical issue in npm dependency (see audit jobs)
- Depending on requirements: HPA

### Nice-to-have

- Add [OCI image labels](https://github.com/opencontainers/image-spec/blob/master/annotations.md) in CI (waiting for feature to be [supported by Kaniko](https://github.com/GoogleContainerTools/kaniko/issues/465))
- Extract Chart to make it re-usable
- Extract Kubernetes deployment functionality into a separate image (pre-installed plugins, script included as shorthand for the long commands)
- CI to be extracted into re-usable templates
- API gateway in front of the service instead of simple Ingress
- OpenAPI specifcation for the available endpoints
- Automated releases based on Conventional Commits (e.g. [standard-version](https://github.com/conventional-changelog/standard-version))
- Integration with observability tools like Jaeger
